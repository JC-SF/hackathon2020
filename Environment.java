import java.util.*;
public class Environment{
	private double xAxis; //where units of 1 is one metre.
	private double yAxis;
	private ArrayList<Person> population;
	private double radiusInfection; //minimum distance of any two points with no risk of infection
	private final double CONTAGION;
	public int days;
	private double uniformDistance;
    // private double R

	public Environment(double xAxis, double yAxis, double radiusInfection, double contagion, int numberOfPeople){ //NOT DONE
		this.xAxis = xAxis;
		this.yAxis = yAxis;
		this.radiusInfection = radiusInfection;
		this.population = new ArrayList<Person>();
		this.CONTAGION = contagion;
		for(int i = 0; i<numberOfPeople; i++) {
			Person person = new Person(i,-1,-1,"uninfected");
			population.add(person);
		}
		this.days = 0;
		this.uniformDistance = -1;
	}
	public boolean createScenarioUniformDistance(double distance) {
		uniformDistance = distance;
		int index = 0;
		boolean valid = false;
		if(days ==-1 && Math.floor((xAxis*yAxis)/distance)>= this.population.size()) {
			for (double i = 0; i<this.xAxis; i+=distance) {
				for(double j = 0; j<this.yAxis; j+=distance) {
					if(index<this.population.size()) {
						Person person = population.get(index);
						person.setXPosition(i);
						person.setYPosition(j);
						index++;
					}
					else {
						break;
					}
				}
			}
			for(Person current : population) {
				current.clearNeighbours();
				for(Person neighbour : population) {
					if(current.getDist(neighbour) < radiusInfection) {
						current.addNeighbour(neighbour);
					}
				}
			}
			if(this.days == 0) {
				Random rand =  new Random();
				int infected = rand.nextInt(population.size());
				this.population.get(infected).getsInfected(CONTAGION);
			}
			valid = true;
		}
		return valid;
	}
	public void createScenarioRandomDistance(){
		if(days == -1) {
			uniformDistance = -1;
			for (Person person : population) {
				person.setXPosition(Math.random()*xAxis);
				person.setYPosition(Math.random()*yAxis);
			}
			for(Person current : population) {
				current.clearNeighbours();
				for(Person neighbour : population) {
					if(current.getDist(neighbour) < radiusInfection) {
						current.addNeighbour(neighbour);
					}
				}
				if(days == 0) {
					Random rand =  new Random();
					int infected = rand.nextInt(population.size());
					this.population.get(infected).getsInfected(CONTAGION);
				}
			}
		}
	}
	public void nextDayRandomDistance() {
		
		for(Person person : population) {
			person.nextDay(radiusInfection, CONTAGION);
		}
		//compute new random position
		days++;
		
	}
	public void nextDayUniformDistance(double uniformDistance) {
		for(Person person : population) {
			person.nextDay(radiusInfection, CONTAGION);
		}
		days++;
	}
	public void reset(){
		for(Person person : population) {
			person.reset();
		}
	}
	public String toString() {
		int infected = 0;
		int recovered = 0;
		int uninfected = 0;
		String state = "";
		for(Person person : this.population) {
			state = person.getState();
			if(state.equals("uninfected")) {
				uninfected++;
			}
			else if(state.equals("infected")) {
				infected++;
			}
			else if(state.equals("recovered")) {
				recovered++;
			}
		}

		String results = "DAY "+days+"\nTotal number of people: "+population.size();
		results+="\nTotal number of uninfected people: "+uninfected;
		results+= "\nTotal number of currently infected people: "+infected;
		results+="\nTotal number of recovered people: "+recovered+"\n";
		return results;
	}
	public void printGraph() {
		System.out.println(this.population.size());
		for(Person person : this.population) {
			System.out.println(person);
		}
	}
}