public class Simulation{
	// make an environment, and populate it
	// there should be n people, n-1 of which are uninfected, and 1 is Patient 0.
	public static void main(String[] args){
		//public Environment(double xAxis, double yAxis, double radiusInfection, double contagion, int numberOfPeople)
		Environment situ1 = new Environment(200,200,2,0.75,1000); //maximum number of  people is then 200 people. Uniform distribution. Expect no one is infected and infection dies off.
		System.out.println(situ1);
		int days = 20;
		situ1.createScenarioUniformDistance(1.8);
		for (int i=1; i<days; i++) {
			situ1.nextDayUniformDistance(1.8);
		}
		System.out.println(situ1);
		//Environment situ2 = new Environment(400, 400, 2, 0.75, 400); //maximum number of  people is then 200 people. Uniform distriution. People are now placed at one mtere distance from each other (room half full)
		//Environment situ3 = new Environment(400,400, 2, 0.75, 500); //maximum number of  people is then 200 people. People are placed 0.0<x<2.0 metres from each other randomly.
	}		
}