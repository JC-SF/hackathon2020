import java.util.*;
public class Person{
	private final int ID;
	private double posX; //x-position of a given Person
	private double posY; //y-position of a given Person
	private String state; //a person can be uninfected, infected, or recovered
	private double contagion; //percentage
	private int daysInfected; //number of days since infection - caps at 14 then Person.state is set recovered 
	public ArrayList<Person> neighbours;

	public Person (int id, double posX, double posY, String state){
		this.ID = id;
		this.posX=posX;
		this.posY=posY;
		this.state=state;
		this.daysInfected=0;
		this.contagion=0.0;
		this.neighbours = new ArrayList<Person>();
	}
	public String getState(){
		return this.state;
	}
	public double getXPosition() {
		return this.posX;
	}
	public double getYPosition() {
		return this.posY;
	}
	public void setXPosition(double x) {
		this.posX = x;
	}
	public void setYPosition(double y){
		this.posY = y;
	}
	public void reset() {
		this.state = "uninfected";
		this.daysInfected = 0;
		this.contagion = 0;
		this.neighbours.clear();
	}
	public double getDist(Person neighbour){
		double diffX = Math.pow(this.posX-neighbour.posX,2);
		double diffY = Math.pow(this.posY-neighbour.posY,2);
		double dist = Math.sqrt(diffX+diffY);
		return dist;
	}
	public void nextDay(double radiusInfection, double contagion){
		//increments day of a person and alters their level of contagiousness
		if(this.state.equals("infected")){
			this.daysInfected++;
			if(this.daysInfected>14){
				this.state = "recovered";
				this.contagion = 0;
				this.daysInfected = -1;
			}
			else {
				//starts 75% chance of infection down to 0%.
				this.contagion -= 0.0535714285714286;
				for(Person person : neighbours) {
					this.applyChancesOfInf(person, radiusInfection, contagion);

				}
			}

		}
		//Implements percentage of contagion that an infected person has.*******
	}
	// called on a person who is infected and uses their distance to determine how likely the any other person is to getting the disease. NOT EFFICIENT TO GO FROM PATIENT0OUTWARD (DEGREEOUT) MAYBE BETTER DEGREE IN
	public void applyChancesOfInf(Person comPerson, double radiusInfection, double contagion){ //NOT DONE
		if(this.state.equals("infected")) {
			double chances = this.contagion;
			chances *= 1-this.getDist(comPerson)/radiusInfection;
			double actualEvent = Math.random(); // generate a number from 0 to 1 non including 1.
			if(actualEvent < chances) {
				comPerson.getsInfected(contagion);
			}
		}

	}
	public void getsInfected(double contagion){ //NOT DONE
		if(this.state == "uninfected"){
			this.state = "infected";
			this.contagion = contagion;
		}
	}
	public void addNeighbour(Person neighbour){
		if (this.ID != neighbour.ID) {
			this.neighbours.add(neighbour);
			neighbour.neighbours.add(this);
		}
	}
	public void clearNeighbours() {
		this.neighbours.clear();
	}
	public String toString() {
		return "ID: "+ID+"\nx: "+posX+"\ny: "+posY+"\nState: "+state;
	}
}